class CardModel {
  String issuedTitle;
  String name;
  String number;
  String icon;
  String validThru;
  double balance;

  CardModel(
      this.issuedTitle, this.name, this.number, this.validThru, this.balance, this.icon);
}
