class TransactionModel {
  int id;
  String name;
  String icon;
  double value;
  int type;
  String time;

  TransactionModel(this.id, this.name, this.icon, this.value,
      this.type, this.time);
}
