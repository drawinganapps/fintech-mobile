import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.light,
    scaffoldBackgroundColor: ColorHelper.dark,
    highlightColor: ColorHelper.dark,
    splashColor: ColorHelper.dark,
    textTheme: GoogleFonts.arimoTextTheme().copyWith(
    ), colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.amber).copyWith(background: ColorHelper.dark),
);
