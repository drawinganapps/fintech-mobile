import 'package:fintech_mobile/routes/AppRoutes.dart';
import 'package:fintech_mobile/screens/home_screen.dart';
import 'package:fintech_mobile/screens/transaction_screen.dart';
import 'package:go_router/go_router.dart';

class AppPage {
// GoRouter configuration
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.TRANSACTION,
        builder: (context, state) => const TransactionScreen(),
      )
    ],
  );
}
