import 'package:fintech_mobile/models/card_model.dart';
import 'package:fintech_mobile/models/transaction_model.dart';

class DummyData {
  static List<TransactionModel> items = [
    TransactionModel(5, 'Spotify', 'spotify', 5.5, 2, '20:15'),
    TransactionModel(1, 'Netflix', 'netflix', 9.9, 2, '10:09'),
    TransactionModel(7, 'Youtube', 'youtube', 9.9, 2, '20:15'),
    TransactionModel(3, 'Playstation', 'buttons', 9.9, 2, '13:21'),
    TransactionModel(4, 'Xbox', 'xbox', 95.2, 2, '16:45'),
    TransactionModel(6, 'Playstation', 'buttons', 9.9, 2, '20:15'),
    TransactionModel(2, 'Steam', 'steam', 129.9, 2, '11:55'),
  ];

  static List<CardModel> cards = [
    CardModel('Visa', 'AHMAD MIRLAN', '6577 6545 4564 4353', '12/2025',  230.30, 'visa.png'),
    CardModel('Master Card', 'AHMAD MIRLAN', '5676 9788 8089 2342', '08/2026',  470.00, 'mastercard.png'),
    CardModel('Master Card', 'AHMAD MIRLAN', '4354 5465 2343 8766', '02/2026',  30.00, 'mastercard.png'),
  ];
}
