import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/models/card_model.dart';
import 'package:fintech_mobile/models/transaction_model.dart';
import 'package:fintech_mobile/sources/dummy_data.dart';
import 'package:fintech_mobile/widgets/card_widget.dart';
import 'package:fintech_mobile/widgets/slide_bullet_indicator_widget.dart';
import 'package:fintech_mobile/widgets/transaction_card_widget.dart';
import 'package:flutter/material.dart';

class TransactionPage extends StatelessWidget {
  const TransactionPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    List<TransactionModel> items = DummyData.items;
    List<CardModel> cards = DummyData.cards;

    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('My Cards',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600)),
              Row(
                children: [
                  Text('See All',
                      style: TextStyle(color: ColorHelper.white, fontSize: 12)),
                  Container(
                    margin: EdgeInsets.only(left: widthSize * 0.01),
                    child: Icon(Icons.library_add,
                        color: ColorHelper.primary, size: widthSize * 0.04),
                  )
                ],
              )
            ],
          ),
        ),
        SizedBox(
          height: heightSize * 0.18,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: List.generate(cards.length, (index) {
              return Container(
                padding: EdgeInsets.only(left: widthSize * 0.03),
                child: CardWidget(card: cards[index]),
              );
            }),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.03),
          child: const SlideBulletIndicatorWidget(),
        ),
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Transaction History',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600)),
              Text('See All',
                  style: TextStyle(color: ColorHelper.primary, fontSize: 12))
            ],
          ),
        ),
        Expanded(
          child: ListView(
            physics: const BouncingScrollPhysics(),
            children: List.generate(items.length, (index) {
              return Container(
                padding: EdgeInsets.only(
                    left: widthSize * 0.05, right: widthSize * 0.05),
                margin: EdgeInsets.only(bottom: heightSize * 0.015),
                child: TransactionCardWidget(item: items[index]),
              );
            }),
          ),
        )
      ],
    );
  }
}
