import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/models/transaction_model.dart';
import 'package:fintech_mobile/sources/dummy_data.dart';
import 'package:fintech_mobile/widgets/balance_card_widget.dart';
import 'package:fintech_mobile/widgets/transaction_card_widget.dart';
import 'package:flutter/cupertino.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    List<TransactionModel> items = DummyData.items;
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.05),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          height: heightSize * 0.15,
          child: const BalanceCardWidget(),
        ),
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.03),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Activity',
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w600)),
              Text('See All',
                  style: TextStyle(
                      color: ColorHelper.primary,
                      fontSize: 12))
            ],
          ),
        ),
        Expanded(
          child: ListView(
            physics: const BouncingScrollPhysics(),
            children: List.generate(items.length, (index) {
              return Container(
                padding:
                EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
                margin: EdgeInsets.only(bottom: heightSize * 0.015),
                child: TransactionCardWidget(item: items[index]),
              );
            }),
          ),
        )
      ],
    );
  }
}
