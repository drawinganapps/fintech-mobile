import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color grey = const Color.fromRGBO(237, 227, 219, 1);
  static Color primary = const Color.fromRGBO(222, 115, 38, 1.0);
  static Color secondary = const Color.fromRGBO(226, 133, 66, 1);
  static Color lightGreen = const Color.fromRGBO(37, 155, 134, 1);
  static Color darkGreen = const Color.fromRGBO(61, 107, 49, 1);
  static Color dark = const Color.fromRGBO(31, 31, 31, 1);
  static Color lightDark = const Color.fromRGBO(38, 38, 38, 1);
  static Color red = const Color.fromRGBO(231, 8, 18, 1);
}
