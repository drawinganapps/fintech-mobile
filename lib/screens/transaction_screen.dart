import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/pages/transaction_page.dart';
import 'package:fintech_mobile/widgets/bottom_navigation_widget.dart';
import 'package:flutter/material.dart';

class TransactionScreen extends StatelessWidget {
  const TransactionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.dark,
        elevation: 0,
        leadingWidth: 30,
        title: Text('Transactions', style: TextStyle(
          color: ColorHelper.white
        )),
        centerTitle: true,
        leading: Container(
          padding: const EdgeInsets.all(5),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: ColorHelper.lightDark
          ),
          child: Icon(Icons.arrow_back_ios, color: ColorHelper.white, size: 25),
        ),
        actions: [
          Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: ColorHelper.lightDark
            ),
            child: Icon(Icons.grid_view_rounded, color: ColorHelper.white, size: 25),
          ),
        ],
      ),
      body: const TransactionPage(),
      bottomNavigationBar: const BottomNavigationWidget(selectedMenu: 2),
    );
  }

}