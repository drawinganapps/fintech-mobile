import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/pages/home_page.dart';
import 'package:fintech_mobile/widgets/bottom_navigation_widget.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.dark,
        elevation: 0,
        leadingWidth: 150,
        leading: Row(
          children: [
            Container(
              decoration: const BoxDecoration(
                  shape: BoxShape.circle
              ),
              clipBehavior: Clip.antiAlias,
              child: Image.asset('assets/images/profile.png', width: 55, fit: BoxFit.fitWidth),
            ),
            Row(
              children: [
                Text('Hello, ', style: TextStyle(
                  color: ColorHelper.white.withOpacity(0.8),
                  fontWeight: FontWeight.w400
                )),
                Text('Mirlan', style: TextStyle(
                    color: ColorHelper.white,
                    fontWeight: FontWeight.w600
                ))
              ],
            )
          ],
        ),
        actions: [
          Container(
            padding: EdgeInsets.only(right: widthSize * 0.03),
            child: Icon(Icons.notifications_none_rounded, color: ColorHelper.white, size: 25),
          ),
          Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: ColorHelper.lightDark
            ),
            child: Icon(Icons.grid_view_rounded, color: ColorHelper.white, size: 25),
          ),
        ],
      ),
      body: const HomePage(),
      bottomNavigationBar: const BottomNavigationWidget(selectedMenu: 1),
    );
  }

}