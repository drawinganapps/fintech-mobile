import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/models/card_model.dart';
import 'package:flutter/cupertino.dart';

class CardWidget extends StatelessWidget {
  final CardModel card;

  const CardWidget({super.key, required this.card});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;

    return Container(
      width: widthSize * 0.7,
      padding: EdgeInsets.all(widthSize * 0.05),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: card.issuedTitle == 'Visa'
                  ? [
                      const Color.fromRGBO(58, 123, 213, 1),
                      const Color.fromRGBO(58, 96, 115, 1),
                    ]
                  : [
                      const Color.fromRGBO(211, 131, 18, 1),
                      const Color.fromRGBO(168, 50, 121, 1),
                    ]),
          borderRadius: BorderRadius.circular(20)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Current Balance',
                      style: TextStyle(
                          color: ColorHelper.white.withOpacity(0.4),
                          fontWeight: FontWeight.w300)),
                  Container(
                    margin: const EdgeInsets.only(top: 5),
                  ),
                  Text('\$${card.balance}',
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold)),
                ],
              ),
              Image.asset('assets/icons/${card.icon}',
                  width: 60, fit: BoxFit.cover)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 5),
                    child: Text(card.name,
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 13,
                            letterSpacing: 1.5,
                            fontWeight: FontWeight.w300)),
                  ),
                  Text(card.number,
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 12,
                          fontWeight: FontWeight.w300)),
                ],
              ),
              Text(card.validThru,
                  style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 12,
                      fontWeight: FontWeight.w300)),
            ],
          )
        ],
      ),
    );
  }
}
