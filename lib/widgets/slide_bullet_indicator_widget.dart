import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:flutter/cupertino.dart';

class SlideBulletIndicatorWidget extends StatelessWidget {
  const SlideBulletIndicatorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;

    Widget activeSlide = Container(
      margin: EdgeInsets.only(left: widthSize * 0.02, right: widthSize * 0.02),
      decoration: BoxDecoration(
          border: Border.all(color: ColorHelper.white), borderRadius: BorderRadius.circular(5)),
      width: widthSize * 0.08,
      height: widthSize * 0.02,
    );

    Widget slideWidget = Container(
      margin: EdgeInsets.only(left: widthSize * 0.02, right: widthSize * 0.02),
      decoration: BoxDecoration(
          border: Border.all(color: ColorHelper.white), shape: BoxShape.circle),
      width: widthSize * 0.02,
      height: widthSize * 0.02,
    );

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: List.generate(3, (index) {
          return index == 1 ? activeSlide : slideWidget;
        }),
      ),
    );
  }
}
