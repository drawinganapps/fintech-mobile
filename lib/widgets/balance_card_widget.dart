import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:flutter/material.dart';

class BalanceCardWidget extends StatelessWidget {
  const BalanceCardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding:
      EdgeInsets.all(widthSize * 0.05),
      decoration: BoxDecoration(
        color: ColorHelper.lightDark,
        borderRadius: BorderRadius.circular(15)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Your balance', style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: widthSize * 0.05,
                color: ColorHelper.white.withOpacity(0.8)
              )),
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: ColorHelper.primary),
                  borderRadius: BorderRadius.circular(5)
                ),
                child: Icon(Icons.add, color: ColorHelper.primary),
              )
            ],
          ),
          Container(
            child: Text('\$1030,20', style: TextStyle(
              color: ColorHelper.white,
              fontWeight: FontWeight.w600,
              fontSize: widthSize * 0.08,
            )),
          )
        ],
      ),
    );
  }

}