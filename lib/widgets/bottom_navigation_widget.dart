import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/routes/AppRoutes.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class BottomNavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const BottomNavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return SizedBox(
      height: heightSize * 0.09,
      child: Container(
        decoration: BoxDecoration(
          color: ColorHelper.dark,
        ),
        padding:
            EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () => {
                context.go(AppRoutes.HOME)
              },
              child: Container(
                decoration: BoxDecoration(shape: BoxShape.circle, color: ColorHelper.lightDark),
                padding: EdgeInsets.all(heightSize * 0.01),
                child: Icon(Icons.home_outlined,
                    size: 25,
                    color: selectedMenu == 1 ? ColorHelper.primary : ColorHelper.white),
              ),
            ),
            GestureDetector(
              onTap: () => {
                context.go(AppRoutes.TRANSACTION)
              },
              child: Container(
                decoration: BoxDecoration(shape: BoxShape.circle, color: ColorHelper.lightDark),
                padding: EdgeInsets.all(heightSize * 0.01),
                child: Icon(Icons.credit_card_outlined,
                    size: 25,
                    color: selectedMenu == 2 ? ColorHelper.primary : ColorHelper.white),
              ),
            ),
            Container(
              decoration: BoxDecoration(shape: BoxShape.circle, color: ColorHelper.lightDark),
              padding: EdgeInsets.all(heightSize * 0.01),
              child: Icon(Icons.receipt_long_outlined,
                  size: 25,
                  color: selectedMenu == 3 ? ColorHelper.primary : ColorHelper.white),
            ),
            Container(
              decoration: BoxDecoration(shape: BoxShape.circle, color: ColorHelper.lightDark),
              padding: EdgeInsets.all(heightSize * 0.01),
              child: Icon(Icons.show_chart_outlined,
                  size: 25,
                  color: selectedMenu == 4 ? ColorHelper.primary : ColorHelper.white),
            ),
            Container(
              decoration: BoxDecoration(shape: BoxShape.circle, color: ColorHelper.lightDark),
              padding: EdgeInsets.all(heightSize * 0.01),
              child: Icon(Icons.person_outline,
                  size: 25,
                  color: selectedMenu == 5 ? ColorHelper.primary : ColorHelper.white),
            ),
          ],
        ),
      ),
    );
  }
}
