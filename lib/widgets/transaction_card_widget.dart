import 'package:fintech_mobile/helper/color_helper.dart';
import 'package:fintech_mobile/models/transaction_model.dart';
import 'package:flutter/cupertino.dart';

class TransactionCardWidget extends StatelessWidget {
  final TransactionModel item;

  const TransactionCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      height: heightSize * 0.1,
      decoration: BoxDecoration(
        color: ColorHelper.lightDark,
        borderRadius: BorderRadius.circular(15)
      ),
      padding: EdgeInsets.all(widthSize * 0.02),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: widthSize * 0.03),
                child: Image.asset('assets/icons/${item.icon}.png'),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(item.name, style: TextStyle(
                    color: ColorHelper.white,
                    fontWeight: FontWeight.w600
                  )),
                  Container(
                    margin: const EdgeInsets.only(top: 5, bottom: 5),
                  ),
                  Text(item.time, style: TextStyle(
                      color: ColorHelper.white.withOpacity(0.8),
                      fontSize: 11,
                      fontWeight: FontWeight.w400
                  )),
                ],
              )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('\$${item.value}', style: TextStyle(
                  color: ColorHelper.white,
                  fontWeight: FontWeight.w600
              )),
              Container(
                margin: const EdgeInsets.only(top: 5, bottom: 5),
              ),
              Text('Subscription', style: TextStyle(
                  color: ColorHelper.primary.withOpacity(0.8),
                  fontSize: 11,
                  fontWeight: FontWeight.w400,
                fontStyle: FontStyle.italic
              )),
            ],
          )
        ],
      ),
    );
  }

}